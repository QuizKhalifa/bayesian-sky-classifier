package main

import (
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io/ioutil"
	"log"
	"math/rand"
)

// TODO: Add the option to use a default image if a matching gt is not found (completely black)
// TODO: Manage different file extension of gt img.
// TODO: Make the anonymous structs actually structs

type bayesColor struct {
	Initialized          bool
	threshold            float64
	alreadyTrainedOn     []string
	dataset1             string
	dataset2             string
	Blocks               [][]string
	BlocksGT             [][]string
	trainingSet          dataset
	q                    int
	withGT               bool
	positiveHistogram    [][][]int
	negativeHistogram    [][][]int
	positivePixelCounter int
	negativePixelCounter int
}

type dataset struct {
	dataset1 []string
	dataset2 []string
}

func NewBayesColor(dataset1, dataset2 string, Q int, withGT bool) bayesColor {
	return bayesColor{
		Initialized:      false,
		threshold:        0.8,
		alreadyTrainedOn: []string{},
		dataset1:         dataset1,
		dataset2:         dataset2,
		q:                Q,
		withGT:           withGT,
		trainingSet:      buildListOfImages(dataset1, dataset2, withGT),
	}
}

func (bc *bayesColor) SetDatasets(dataset1, dataset2 string) {
	bc.dataset1 = dataset1
	bc.dataset2 = dataset2
	bc.trainingSet = buildListOfImages(dataset1, dataset2, bc.withGT)
}

func (bc *bayesColor) SetQ(q int) {
	bc.q = q
}

func (bc *bayesColor) SetWithGT(withGT bool) {
	bc.withGT = withGT
}

/* func (bc *bayesColor) RemoveValidationFromTraining(equalTrainingSetSize bool) {
	for _, val := range bc.validationSet.dataset1 {
		for i, train := range bc.trainingSet.dataset1 {
			p := strings.Split(val, "/")
			g := strings.Split(train, "/")
			if p[len(p)-1] == g[len(g)-1] {
				bc.trainingSet.dataset1[i] = bc.trainingSet.dataset1[len(bc.trainingSet.dataset1)-1]
				bc.trainingSet.dataset1 = bc.trainingSet.dataset1[:len(bc.trainingSet.dataset1)-1]
				break
			}
		}
	}
	if equalTrainingSetSize {
		bc.trainingSet.dataset2 = bc.trainingSet.dataset2[:len(bc.trainingSet.dataset1)]
	}
} */

/*
Creates a histogram that can be used by the algorithm to classify colors
*/
func (bc *bayesColor) Train(threads, kForValidation int) {
	if bc.withGT {
		log.Printf("Training set consists of %d images\n", len(bc.trainingSet.dataset1)-len(bc.Blocks[kForValidation]))
	} else {
		log.Printf("Dataset consists of %d images\n", (len(bc.trainingSet.dataset1)-len(bc.Blocks[kForValidation]))+len(bc.trainingSet.dataset2))
	}

	// Initialize slices and counters
	bc.positiveHistogram = make([][][]int, int(256/bc.q))
	bc.negativeHistogram = make([][][]int, int(256/bc.q))
	for i := range bc.positiveHistogram {
		bc.positiveHistogram[i] = make([][]int, int(256/bc.q))
		bc.negativeHistogram[i] = make([][]int, int(256/bc.q))
		for j := range bc.positiveHistogram[i] {
			bc.positiveHistogram[i][j] = make([]int, int(256/bc.q))
			bc.negativeHistogram[i][j] = make([]int, int(256/bc.q))
		}
	}

	bc.positivePixelCounter = 0
	bc.negativePixelCounter = 0

	log.Printf("%s", "Began Training")

	/********************************************\
	|		Let's go threading baby!			 |
	\********************************************/

	// Create a channel for multithreading
	c := make(chan []struct {
		positive bool
		r        uint8
		g        uint8
		b        uint8
	})
	image_index := make(chan struct {
		a int
		b int
	})

	// A thread that saturates the other threads
	go func(next_image chan struct {
		a int
		b int
	}) {
		for k, i := range bc.Blocks {
			if k == kForValidation {
				continue
			}
			for j := range i {
				next_image <- struct {
					a int
					b int
				}{
					a: k,
					b: j,
				}
			}
		}
		close(next_image)
	}(image_index)

	// Create saturation thread for the other dataset
	image_index2 := make(chan int)

	if !bc.withGT {
		// A thread that saturates the other threads
		go func(c chan []struct {
			positive bool
			r        uint8
			g        uint8
			b        uint8
		}, image_index chan int) {
			for i := range bc.trainingSet.dataset2 {
				image_index <- i
			}
			close(image_index)
		}(c, image_index2)
	}

	done := make(chan bool)

	go func(c chan []struct {
		positive bool
		r        uint8
		g        uint8
		b        uint8
	}, done chan bool) {
		doneCounter := 0
		for range done {
			doneCounter++
			if doneCounter == threads {
				close(c)
			}
		}
	}(c, done)

	// Read the pixels by means of multithreading
	// splitting the dataset by the length of it divided by the number of threads
	for i := 0; i < threads; i++ {

		if bc.withGT {
			go bc.countPixelsWithGT(bc.trainingSet, c, image_index, done)
		} else {
			// the gt set now points to the non-positive set
			go bc.countPixelsNoGT(c, image_index, image_index2, done)
		}

	}

	// Receive the counted values

	for v := range c {
		for _, a := range v {
			if a.positive {
				bc.positiveHistogram[a.r][a.g][a.b]++
				bc.positivePixelCounter++
			} else {
				bc.negativeHistogram[a.r][a.g][a.b]++
				bc.negativePixelCounter++
			}
		}
	}
	log.Printf("%s", "Training is done!")
	bc.Initialized = true
}

// Sets the validation sets.
// pathToDataset is used when we don't train with the GT
// First entry is the folder with images to classify
// Second entry is to the GT of those images
func (bc *bayesColor) SetKFoldBlocks(folds int) {
	blockLen := int(len(bc.trainingSet.dataset1) / folds)

	bc.Blocks = make([][]string, folds)
	bc.BlocksGT = make([][]string, folds)

	trainingSet1 := bc.trainingSet.dataset1
	trainingSet2 := bc.trainingSet.dataset2

	for i := 0; i < folds; i++ {
		bc.Blocks[i] = make([]string, 0, blockLen)

		for j := 0; j < blockLen; j++ {
			if len(trainingSet1) < blockLen {
				bc.Blocks[i] = trainingSet1
				bc.BlocksGT[i] = trainingSet2
				return
			} else {
				index := rand.Intn(len(trainingSet1) - 1)

				bc.Blocks[i] = append(bc.Blocks[i], trainingSet1[index])
				bc.BlocksGT[i] = append(bc.BlocksGT[i], trainingSet2[index])

				// Remove it from the training sets
				trainingSet1[index] = trainingSet1[len(trainingSet1)-1]
				trainingSet1 = trainingSet1[:len(trainingSet1)-1]

				trainingSet2[index] = trainingSet2[len(trainingSet2)-1]
				trainingSet2 = trainingSet2[:len(trainingSet2)-1]
			}
		}
	}
}

/*
A helper function for the training. It will go through and count
all the pixels within a set of images and returns a histogram of positive (e.g. skin) and
negative (e.g. non-skin) colors. It requires a ground truth for each image that is provided.
The name of the image files should be the same, but the extension can be different.
*/
func (bc *bayesColor) countPixelsWithGT(imageList struct {
	dataset1 []string
	dataset2 []string
}, c chan []struct {
	positive bool
	r        uint8
	g        uint8
	b        uint8
}, image_index chan struct {
	a int
	b int
}, done chan bool) {

	// Slight speed upgrade compared to reallocating every loop. Uses more memory
	temp := make([]struct {
		positive bool
		r        uint8
		g        uint8
		b        uint8
	}, 0, 5000*5000)

	for v := range image_index {
		// Empty the slice without freeing the underlying memory alloc
		temp = temp[:0]

		// Load image
		img := loadImage(bc.Blocks[v.a][v.b])

		// Load ground truth
		imgGT := loadImage(bc.BlocksGT[v.a][v.b])

		/////////////////

		for x := 0; x < img.Bounds().Max.X; x++ {
			for y := 0; y < img.Bounds().Max.Y; y++ {

				// Get the color of the pixel
				r, g, b, _ := img.At(x, y).RGBA()
				r, g, b = (r/256)/uint32(bc.q), (g/256)/uint32(bc.q), (b/256)/uint32(bc.q)

				// We don't care about blue in the ground truth for now
				rGT, _, _, _ := imgGT.At(x, y).RGBA()
				rGT /*, gGT*/ = (rGT / 256) /*, (gGT / 256) */
				// Count it
				/*
					If the ground truth says it's skin
					If red AND green, then it's white, and it's positive
				*/
				if rGT > 100 /*&& gGT > 100*/ {

					temp = append(temp, struct {
						positive bool
						r        uint8
						g        uint8
						b        uint8
					}{positive: true, r: uint8(r), g: uint8(g), b: uint8(b)})

					// If only red is greater then 100,
					// then it's red and it's difficult to classify the pixel
					// (TODO: Figure out what to do here)

				} else {
					// Otherwise the pixel is negative
					temp = append(temp, struct {
						positive bool
						r        uint8
						g        uint8
						b        uint8
					}{positive: false, r: uint8(r), g: uint8(g), b: uint8(b)})
				}
			}
		}
		c <- temp
	}

	done <- true
}

func (bc *bayesColor) countPixelsNoGT(c chan []struct {
	positive bool
	r        uint8
	g        uint8
	b        uint8
}, image_index chan struct {
	a int
	b int
}, image_index2 chan int, done chan bool) {

	bc.countPositivePixels(c, image_index)
	bc.countNegativePixels(c, image_index2)
	done <- true
}

func (bc *bayesColor) countPositivePixels(c chan []struct {
	positive bool
	r        uint8
	g        uint8
	b        uint8
}, image_index chan struct {
	a int
	b int
}) {
	// Slight speed upgrade compared to reallocating every loop. Uses more memory
	// Potential problem: Might not work on larger images
	holder := make([]struct {
		positive bool
		r        uint8
		g        uint8
		b        uint8
	}, 0, 5000*5000)

	for v := range image_index {

		// Empty the slice without freeing the underlying memory alloc
		holder = holder[:0]

		// Load image
		img := loadImage(bc.Blocks[v.a][v.b])

		holder = *bc.countPixelsImage(&holder, &img, true)
		c <- holder
	}
}

func (bc *bayesColor) countNegativePixels(c chan []struct {
	positive bool
	r        uint8
	g        uint8
	b        uint8
}, image_index chan int) {
	// Slight speed upgrade compared to reallocating every loop. Uses more memory
	// Potential problem: Might not work on larger images
	holder := make([]struct {
		positive bool
		r        uint8
		g        uint8
		b        uint8
	}, 0, 5000*5000)

	for i := range image_index {

		// Empty the slice without freeing the underlying memory alloc
		holder = holder[:0]

		// Load image
		img := loadImage(bc.trainingSet.dataset2[i])

		holder = *bc.countPixelsImage(&holder, &img, false)
		c <- holder
	}
}

// Looks at the surrounding colors and gets the average sum
func (bc *bayesColor) smoothing_filter(positiveHistogram bool, r, g, b, factor int) int {
	if factor == 1 {
		if positiveHistogram {
			return bc.positiveHistogram[r][g][b]
		} else {
			return bc.negativeHistogram[r][g][b]
		}
	}

	topLeft := struct {
		r int
		g int
		b int
	}{r: r - (factor - 2), g: g - (factor - 2), b: b - (factor - 3)}

	sum := 0
	nrOfNumbers := 0 // Should end up being 27 if factor == 3, but can be smaller

	for i := 0; i < factor; i++ {
		for j := 0; j < factor; j++ {
			for k := 0; k < factor; k++ {
				if topLeft.r+i < 0 || topLeft.g+j < 0 || topLeft.b+k < 0 || topLeft.r+i > 255/Q || topLeft.g+j > 255/Q || topLeft.b+k > 255/Q {
					continue
				}
				if positiveHistogram {
					sum += bc.positiveHistogram[topLeft.r+i][topLeft.g+j][topLeft.b+k]
				} else {
					sum += bc.negativeHistogram[topLeft.r+i][topLeft.g+j][topLeft.b+k]
				}
				nrOfNumbers++
			}
		}
	}
	return sum / nrOfNumbers
}

// Returns true if the r,g,b combinations is classified as positive
func (bc *bayesColor) Classify(r, g, b uint32) bool {
	finalProbabilityOfPositive := bc.PositiveProbability(int(r), int(g), int(b))

	return finalProbabilityOfPositive > bc.threshold
}

func (bc *bayesColor) PositiveProbability(r, g, b int) float64 {
	r, g, b = int(r/bc.q), int(g/bc.q), int(b/bc.q)
	positive_pixels := bc.smoothing_filter(true, r, g, b, 3)
	negative_pixels := bc.smoothing_filter(false, r, g, b, 3)

	// The threhold we use to say if it is positive or not
	if negative_pixels == 0 && positive_pixels > 0 { // We've only seen positive versions of this
		return 1
	} else if positive_pixels == 0 && negative_pixels > 0 { // We've only seen negative version of this
		return 0
	} else if negative_pixels == 0 && positive_pixels == 0 {
		return 0.1 // The question is: if we've never seen it before, is it likely to be positive or negative? Probably negative.
	}
	p_of_positive := float64(positive_pixels) / float64(negative_pixels)
	p_of_negative := float64(negative_pixels) / float64(positive_pixels)

	// The 0.5 value is supposed to be a cost to prevent false positive.
	// Is usually assumed to be 0.5, but can be estimated based on the number of
	// pixels in both sets.
	prior_positve_prob := 0.5 //float64(bc.positivePixelCounter)/float64(bc.positivePixelCounter) + float64(bc.negativePixelCounter)
	prior_negaive_prob := 0.5 //float64(bc.negativePixelCounter)/float64(bc.negativePixelCounter) + float64(bc.positivePixelCounter)

	return (p_of_positive * prior_positve_prob) / (p_of_positive*prior_positve_prob + p_of_negative*prior_negaive_prob)
}

// Sets a threshold value (between 0 and 1).
// Threshold decides what probability a pixel should have for being positive
func (bc *bayesColor) SetThreshold(newThreshold float64) {
	bc.threshold = newThreshold
}

func buildListOfImages(folderPath, folderPathGT string, withGT bool) dataset {

	imageList := dataset{
		dataset1: []string{},
		dataset2: []string{},
	}

	if withGT {
		fileList, err := ioutil.ReadDir(folderPath)
		if err != nil {
			log.Fatal("Path to training set does not exist!")
		}
		for _, file := range fileList {
			if file.IsDir() {
				tmp := buildListOfImages(folderPath+file.Name()+"/", folderPathGT+file.Name()+"/", withGT)
				imageList.dataset1 = append(imageList.dataset1, tmp.dataset1...)
				imageList.dataset2 = append(imageList.dataset2, tmp.dataset2...)
			} else {
				imageList.dataset1 = append(imageList.dataset1, folderPath+file.Name())
				imageList.dataset2 = append(imageList.dataset2, folderPathGT+file.Name())
			}
		}
	} else {
		fileList1, err := ioutil.ReadDir(folderPath)
		if err != nil {
			log.Fatal("Path to training set 1 does not exist!")
		}
		fileList2, err := ioutil.ReadDir(folderPathGT)
		if err != nil {
			log.Fatal("Path to training set 2 does not exist!")
		}
		for _, file := range fileList1 {
			if file.IsDir() {
				tmp := buildListOfImages(folderPath+file.Name()+"/", folderPathGT+file.Name()+"/", withGT)
				imageList.dataset1 = append(imageList.dataset1, tmp.dataset1...)
			} else {
				imageList.dataset1 = append(imageList.dataset1, folderPath+file.Name())
			}
		}
		for _, file := range fileList2 {
			if file.IsDir() {
				tmp := buildListOfImages(folderPath+file.Name()+"/", folderPathGT+file.Name()+"/", withGT)
				imageList.dataset2 = append(imageList.dataset2, tmp.dataset2...)
			} else {
				imageList.dataset2 = append(imageList.dataset2, folderPathGT+file.Name())
			}
		}
	}

	log.Printf("Dataset1: %d, Dataset2: %d\n", len(imageList.dataset1), len(imageList.dataset2))

	return imageList
}

func (bc *bayesColor) SetDataset2(path string) {
	bc.trainingSet.SetDataset2(path)
	bc.dataset2 = path
}

func (d *dataset) SetDataset2(path string) {
	d.dataset2 = d.listOfPaths(path)
	log.Printf("Dataset1: %d, Dataset2: %d\n", len(d.dataset1), len(d.dataset2))
}

func (d *dataset) listOfPaths(path string) []string {
	out := make([]string, 0, 50)
	fileList, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal("Path to dataset does not exist!")
	}

	for _, file := range fileList {
		if file.IsDir() {
			tmp := d.listOfPaths(path + file.Name() + "/")
			out = append(out, tmp...)
		} else {
			out = append(out, path+file.Name())
		}
	}

	return out
}

func (bc *bayesColor) countPixelsImage(holder *[]struct {
	positive bool
	r        uint8
	g        uint8
	b        uint8
}, image *image.Image, positive bool) *[]struct {
	positive bool
	r        uint8
	g        uint8
	b        uint8
} {
	img := *image
	height := img.Bounds().Max.Y
	if positive {
		height = height / 2
	}

	for x := 0; x < img.Bounds().Max.X; x++ {
		for y := 0; y < height; y++ {

			// Get the color of the pixel
			r, g, b, _ := img.At(x, y).RGBA()
			r, g, b = (r/256)/uint32(bc.q), (g/256)/uint32(bc.q), (b/256)/uint32(bc.q)

			// An effort to try and remove pixels that certainly is not the sky
			if g > b && g > r {
				continue
			}

			// pixels are registered
			tmp := append(*holder, struct {
				positive bool
				r        uint8
				g        uint8
				b        uint8
			}{positive: positive, r: uint8(r), g: uint8(g), b: uint8(b)})
			holder = &tmp
		}
	}
	return holder
}
