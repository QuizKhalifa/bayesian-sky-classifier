package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"time"
)

var verbose bool = false
var Q int = 1
var probMapPath string = ""
var classifiedPath string = ""
var classifyImagePath string = ""
var classifyNonGT bool = false
var printBest bool = false

func main() {

	// Manage flags
	var skyPath string
	var skyPathGT string
	var noSkyPath string
	var threshold float64
	var threads int
	var folds int

	flag.StringVar(&skyPath, "s", "./sky_pictures/sky/",
		"Specify path to the dataset of sky images.")
	flag.StringVar(&skyPathGT, "g",
		"./sky_pictures/gt/", "Specify path to the dataset of ground truths for the sky images.")
	flag.StringVar(&noSkyPath, "n",
		"./sky_pictures/non-sky/", "Specify path to the dataset of non-sky images.")
	flag.IntVar(&Q, "Q", 1, "Reduce histograms counter by Q (256/Q)")
	flag.Float64Var(&threshold, "f", 0.6,
		"Specify acceptance threshold.")
	flag.IntVar(&threads, "t", 8,
		"Specify number of threads. Mostly affects memory usage when going beyond the CPU's threads")
	flag.IntVar(&folds, "k", 2,
		"Specify how many folds it should do for k-fold validation.")
	flag.BoolVar(&verbose, "v", false, "Enable verbose output")
	flag.StringVar(&probMapPath, "p", "", "Output probability maps of the training set to this folder")
	flag.StringVar(&classifiedPath, "c", "", "Output classified maps of the training set to this folder")
	flag.StringVar(&classifyImagePath, "I", "", "Path to the input file. If this flag is set, then testing is not performed, and the image is classified instead.")
	flag.BoolVar(&classifyNonGT, "u", false, "When classifying a single image, use the non-gt dataset")
	flag.BoolVar(&printBest, "b", false, "Also show which fold was the most successfull and its score")

	flag.Parse()

	if folds < 2 {
		log.Fatalln("Folds must be 2 or more")
	}

	if classifyImagePath == "" && classifyNonGT {
		log.Fatalln("Flag -u cannot be used without specifying -I")
	}

	// First we train with ground truth and evaluate its performance
	// Then we train without ground truths and evaluate its performance
	// This allows us to keep the validation set from the ground truth training

	rand.Seed(0xdeadbeef) // Static seed so it's always the same with the same input

	run(folds, threads, Q, skyPath, skyPathGT, noSkyPath, threshold)
}

func verb(format string, a ...interface{}) {
	if verbose {
		log.Printf(format, a...)
	}
}

func run(folds, threads, Q int, skyPath, skyPathGT, noSkyPath string, threshold float64) {
	cmGT := newConfusionMatrix()
	cmNoGT := newConfusionMatrix()

	bc := NewBayesColor(skyPath, skyPathGT, Q, true)
	bc.SetThreshold(threshold)
	bc.SetKFoldBlocks(folds)
	bc.SetDataset2(noSkyPath)

	// Just classifying a single picture
	if classifyImagePath != "" {
		verb("Classifying...\n")

		if classifyNonGT {
			bc.SetWithGT(false)
		}

		train(&bc, threads, 0)
		dottedColoring(classifyImagePath, "./out.png", bc)
		return
	}

	bestGT := 0
	bestCMGT := &confusionMatrix{Accuracy: 0}
	bestNoGT := 0
	bestCMNoGT := &confusionMatrix{Accuracy: 0}

	start := time.Now()
	for i := 0; i < folds; i++ {
		bc.SetWithGT(true)
		verb("Training and testing with gt...\n")
		train(&bc, threads, i)
		cm := eval(&bc, i, threads)
		cmGT.TP += cm.TP
		cmGT.FP += cm.FP
		cmGT.TN += cm.TN
		cmGT.FN += cm.FN
		cmGT.Total += cm.Total
		cmGT.TotalNegative += cm.TotalNegative
		cmGT.TotalPositive += cm.TotalPositive

		if printBest {
			cm.calculcateMeasures()
			if cm.Accuracy > bestCMGT.Accuracy {
				bestCMGT = cm
				bestGT = i
			}
		}

		if classifiedPath != "" {
			verb("Classifying images for GT")
			cntr := 0
			for _, block := range bc.Blocks {
				for _, path := range block {
					dottedColoring(path, classifiedPath+fmt.Sprint(cntr)+"GT"+".png", bc)
					cntr++
				}
			}
		}

		if probMapPath != "" {
			verb("Probability map for images with GT")
			cntr := 0
			for _, block := range bc.Blocks {
				for _, path := range block {
					createProbabilityMap(path, probMapPath+fmt.Sprint(cntr)+"GT"+".png", bc)
					cntr++
				}
			}
		}

		// Setup for training without GT
		bc.SetWithGT(false)

		verb("Training and testing without gt...\n")
		train(&bc, threads, i)
		cm = eval(&bc, i, threads)
		cmNoGT.TP += cm.TP
		cmNoGT.FP += cm.FP
		cmNoGT.TN += cm.TN
		cmNoGT.FN += cm.FN
		cmNoGT.Total += cm.Total
		cmNoGT.TotalNegative += cm.TotalNegative
		cmNoGT.TotalPositive += cm.TotalPositive

		if printBest {
			cm.calculcateMeasures()
			if cm.Accuracy > bestCMNoGT.Accuracy {
				bestCMNoGT = cm
				bestNoGT = i
			}
		}

		if classifiedPath != "" {
			verb("Classifying images for no GT")
			cntr := 0
			for _, block := range bc.Blocks {
				for _, path := range block {
					dottedColoring(path, classifiedPath+fmt.Sprint(cntr)+"NoGT"+".png", bc)
					cntr++
				}
			}
		}

		if probMapPath != "" {
			verb("Probability map for images with no GT")
			cntr := 0
			for _, block := range bc.Blocks {
				for _, path := range block {
					createProbabilityMap(path, probMapPath+fmt.Sprint(cntr)+"NoGT"+".png", bc)
					cntr++
				}
			}
		}

		classifiedPath = ""
		probMapPath = ""

		verb("Time elapsed so far: %s\n", time.Since(start))
	}

	// Calculate average score
	// as well as the measures
	cmGT.avg(folds)
	cmGT.calculcateMeasures()
	cmNoGT.avg(folds)
	cmNoGT.calculcateMeasures()

	if printBest {
		printCM(bestCMGT, "Best fold CM W/GT: "+fmt.Sprint(bestGT))
		printCM(bestCMNoGT, "Best fold CM W/O GT: "+fmt.Sprint(bestNoGT))
	}

	printCM(&cmGT, "Confusion Matrix W/ GT")
	printCM(&cmNoGT, "Confusion Matrix W/O GT")
}

func train(bc *bayesColor, threads, k int) {
	start := time.Now()
	bc.Train(threads, k)
	verb("\tPositive pixels: %d\n\tNegative pixels: %d\n\tTotal pixels: %d\n",
		bc.positivePixelCounter, bc.negativePixelCounter, bc.positivePixelCounter+bc.negativePixelCounter)

	verb("Training took %s\n", time.Since(start))
}

func eval(bc *bayesColor, k, threads int) *confusionMatrix {
	// Eval with GT
	startEvalGT := time.Now()
	confusionMatrix := evaluate(k, bc, threads, bc.Classify)
	verb("Testing took %s\n", time.Since(startEvalGT))

	return &confusionMatrix
}

func printCM(cm *confusionMatrix, title string) {
	fmt.Printf("#-----------%s----------#\n", title)
	fmt.Printf("TN: %f\tFN: %f\nFP: %f\tTP: %f\nTotal: %f\nActual Total positives: %f\nActual Total negatives: %f\n",
		cm.TN, cm.FN, cm.FP, cm.TP, cm.Total, cm.TotalPositive, cm.TotalNegative)
	fmt.Printf("Accuracy: %f\nPrecision: %f\nRecall: %f\nSpecificity: %f\nF1: %f\nFPR: %f\n\n",
		cm.Accuracy, cm.Precicion, cm.Recall, cm.Specificity, cm.F1, cm.FPR)
}
