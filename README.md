# Bayes classifier

The code is part of a university project. The goal is to use Bayes Classifier to test if ground truths are truly necessary for skin classification/segmentation, or if we can just provide a set of images "that contain skin" and images that don't. The program trains on a dataset with ground truths and compares it to a model it traines without ground truths. It is also possible to just classify a single image.

## Technologies

The program is written purely in Golang with no non-standard libraries. 

[Install Golang](https://go.dev/doc/install) to run the makefile.

The binary is output as `main.out`.

## Usage

At least 16 GB of RAM is recommended for this program. Less RAM is supported, but fewer threads should be used (specify with `-t <int>` tag). It takes 6 minutes and 50 seconds to run the program on an AMD Ryzen 4700u laptop CPU with the dataset at the bottom of the README.

**Note**: The model cannot be saved! It must be regenerated every run!

The project includes a `makefile`. To compile _and_ run the program, use `make run` (flags are pre-specified). To just compile the program, use `make`. The binary must be compiled before user-specifed flags can be set (unless you edit the makefile).

`make run` will execute the program with the following command: `./${BINARY_NAME} -v -Q 4 -k 4 -b`.

To just classify a single picture, use the `-I <input image path>` flag. The output file is named `out.png`.

Flags:
```bash
  -I string
        Path to the input file. If this flag is set, then testing is not performed, and the image is classified instead.
  -Q int
        Reduce histograms counter by Q (256/Q) (default 1)
  -b    Also show which fold was the most successfull and its score
  -c string
        Output classified maps of the training set to this folder
  -f float
        Specify acceptance threshold. (default 0.6)
  -g string
        Specify path to the dataset of ground truths for the sky images. (default "./sky_pictures/gt/")
  -k int
        Specify how many folds it should do for k-fold validation. (default 2)
  -n string
        Specify path to the dataset of non-sky images. (default "./sky_pictures/non-sky/")
  -p string
        Output probability maps of the training set to this folder
  -s string
        Specify path to the dataset of sky images. (default "./sky_pictures/sky/")
  -t int
        Specify number of threads. Mostly affects memory usage when going beyond the CPU\'s threads (default 8)
  -u    
        When classifying a single image, use the non-gt dataset
  -v    
        Enable verbose output
```

## Dataset

A dataset is available [here](https://studntnu-my.sharepoint.com/:u:/g/personal/brorlaus_ntnu_no/Ec7zCyg_dOtMhKHcxUvnTDoBP-ixYwwxTC1EYRSJsqQhDg?e=jUTT1b)