package main

import (
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"log"
	"os"
)

/*
This document is unused, but is useful for testing and visualization purposes
*/

func createProbabilityMap(imagePath, outpath string, bc bayesColor) {
	img := loadImage(imagePath)
	cimg := image.NewGray(img.Bounds())

	for x := 0; x < cimg.Bounds().Max.X; x++ {
		for y := 0; y < cimg.Bounds().Max.Y; y++ {
			r, g, b, _ := img.At(x, y).RGBA()
			r, g, b = r/256, g/256, b/256

			prob := bc.PositiveProbability(int(r), int(g), int(b))
			cimg.Set(x, y, color.Gray{uint8(256 - float64(256*prob))})
		}
	}
	out, err := os.Create(outpath)
	if err != nil {
		log.Fatal("Could not create file to write image to: ", err)
	}

	jpeg.Encode(out, cimg, nil)
	out.Close()
}

func pixelWiseColoring(imagePath, outpath string, bc bayesColor) {
	img := loadImage(imagePath)
	cimg := image.NewRGBA(img.Bounds())
	draw.Draw(cimg, img.Bounds(), img, image.Point{}, draw.Over)

	for x := 0; x < cimg.Bounds().Max.X; x++ {
		for y := 0; y < cimg.Bounds().Max.Y; y++ {
			r, g, b, _ := img.At(x, y).RGBA()
			r, g, b = r/256, g/256, b/256

			if bc.Classify(r, g, b) {
				cimg.Set(x, y, color.RGBA{R: 255, G: 0, B: 0, A: 0})
			}
		}
	}
	out, err := os.Create(outpath)
	if err != nil {
		log.Fatal("Could not create file to write image to: ", err)
	}

	jpeg.Encode(out, cimg, nil)
	out.Close()
}

func dottedColoring(imagePath, outpath string, bc bayesColor) {
	sm := NewSegmenter(imagePath)
	sm.Classify(bc.Classify)

	img := loadImage(imagePath)
	cimg := image.NewRGBA(img.Bounds())
	draw.Draw(cimg, img.Bounds(), img, image.Point{}, draw.Over)

	for _, segment := range sm.positivePartitions {
		for x := segment[0].x; x < segment[2].x; x++ {
			for y := segment[0].y; y < segment[1].y; y++ {
				cimg.Set(x, y, color.RGBA{R: 255, G: 0, B: 0, A: 0})
			}
		}
	}
	out, err := os.Create(outpath)
	if err != nil {
		log.Fatal("Could not create file to write image to: ", err)
	}

	jpeg.Encode(out, cimg, nil)
	out.Close()
}
