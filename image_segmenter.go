package main

import (
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"log"
	"math"
	"os"
	"strings"
)

type segmenter struct {
	img                image.Image
	imgName            string
	dimensions         uint32
	perfNumber         uint32
	imageSize          uint32
	nrOfPartitions     uint32
	positivePartitions [][]pixel
}

type pixel struct {
	x int
	y int
}

type Changeable interface {
	Set(x, y int, c color.Color)
}

func NewSegmenter(imagePath string) segmenter {
	img := loadImage(imagePath)
	imgSize := uint32(img.Bounds().Max.X * img.Bounds().Max.Y)
	var dimensions uint32 = 2
	var perfNumber uint32 = 25
	var nrOfPartitions uint32 = uint32(math.Pow(float64(imgSize)/float64(perfNumber), 1/float64(dimensions)))

	imgPathSplit := strings.Split(imagePath, "/")

	return segmenter{
		img:            img,
		imgName:        imgPathSplit[len(imgPathSplit)-1],
		dimensions:     dimensions,
		perfNumber:     perfNumber,
		imageSize:      imgSize,
		nrOfPartitions: nrOfPartitions,
	}
}

func (s *segmenter) Classify(fn func(uint32, uint32, uint32) bool) {
	partitionSize := int(math.Round(float64(s.imageSize) / float64(s.nrOfPartitions)))
	for partitionSize > int(s.nrOfPartitions) {
		partitionSize = int(math.Round(float64(partitionSize) / float64(s.nrOfPartitions)))
	}

	// TODO: Consider if we need to actually do anything about this
	xPartitionSize := 5
	yPartitionSize := 5

	// Find R and detect skin
	// R are the pixels that we actually look at
	// The pixels in the corners and the pixel in the center
	s.positivePartitions = make([][]pixel, 0, 1000)
	for x := 0; x < int(math.Round(float64(s.img.Bounds().Max.X)/float64(xPartitionSize))); x++ {
		for y := 0; y < int(math.Round(float64(s.img.Bounds().Max.Y)/float64(yPartitionSize))); y++ {
			yUpper := (y + 1) * yPartitionSize
			xUpper := (x + 1) * xPartitionSize

			// Make sure that the upper value is _not larger than the actual image_
			if yUpper >= s.img.Bounds().Max.Y {
				yUpper = s.img.Bounds().Max.Y - 1
			}
			if xUpper >= s.img.Bounds().Max.X {
				xUpper = s.img.Bounds().Max.X - 1
			}

			temp := s.findR(x*xPartitionSize, y*yPartitionSize, xUpper, yUpper)

			if len(s.detectPositive(temp, fn)) == 5 {
				s.positivePartitions = append(s.positivePartitions, temp)
			}
		}
	}
}

func (s *segmenter) findR(lowerX, lowerY, upperX, upperY int) []pixel {
	return []pixel{
		{lowerX, lowerY},
		{lowerX, upperY},
		{upperX, lowerY},
		{upperX, upperY},
		{lowerX + int(math.Round(float64(upperX-lowerX))/2), lowerY + int(math.Round(float64(upperY-lowerY))/2)},
	}
}

func (s *segmenter) detectPositive(R []pixel, fn func(uint32, uint32, uint32) bool) []pixel {
	positivePixels := make([]pixel, 0, 5)

	for _, p := range R {
		r, g, b, _ := s.img.At(p.x, p.y).RGBA()
		r, g, b = r/256, g/256, b/256

		if fn(r, g, b) {
			positivePixels = append(positivePixels, p)
		}
	}

	return positivePixels
}

func (s *segmenter) WriteImage(filepath string) {
	cimg := image.NewRGBA(s.img.Bounds())
	draw.Draw(cimg, s.img.Bounds(), s.img, image.Point{}, draw.Over)

	for _, positive := range s.positivePartitions {
		drawCircle(cimg, positive)
	}
	out, err := os.Create(filepath)
	if err != nil {
		log.Fatal("Could not create file to write image to: ", err)
	}

	jpeg.Encode(out, cimg, nil)
	out.Close()
}

func drawCircle(img *image.RGBA, positive []pixel) {

	radius := int((float64(1) / float64(2)) * float64((positive[2].x - positive[1].x)))
	centerX, centerY := positive[4].x, positive[4].y

	for x := -radius; x <= radius; x++ {
		height := int(math.Sqrt(math.Abs(float64(radius*radius - x*x))))

		for y := -height; y < height; y++ {
			img.Set(x+centerX, y+centerY, color.RGBA{255, 0, 0, 255})
		}
	}
}

func loadImage(path string) image.Image {
	// Load image
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	img, err := jpeg.Decode(f)
	if err != nil {
		// TODO: Be more graceful
		log.Fatalf("Image is not a valid filetype!: %s", path)
	}
	return img
}
