package main

type confusionMatrix struct {
	TP            float64
	FP            float64
	TN            float64
	FN            float64
	Total         float64
	TotalPositive float64
	TotalNegative float64
	Accuracy      float64
	Precicion     float64
	Recall        float64
	Specificity   float64
	F1            float64
	FPR           float64
}

func newConfusionMatrix() confusionMatrix {
	return confusionMatrix{
		TP:            0,
		FP:            0,
		TN:            0,
		FN:            0,
		Total:         0,
		TotalPositive: 0,
		TotalNegative: 0,
		Accuracy:      0,
		Precicion:     0,
		Recall:        0,
		Specificity:   0,
		F1:            0,
		FPR:           0,
	}
}

func (cm *confusionMatrix) avg(k int) {
	i := float64(k)
	cm.FP /= i
	cm.TN /= i
	cm.FN /= i
	cm.TP /= i
	cm.TotalNegative /= i
	cm.TotalPositive /= i
	cm.Total /= i
}

func (cm *confusionMatrix) calculcateMeasures() {
	cm.Accuracy = (float64(cm.TP) + float64(cm.TN)) / (float64(cm.Total))
	cm.Precicion = float64(cm.TP) / (float64(cm.TP) + float64(cm.FP))
	cm.Recall = float64(cm.TP) / (float64(cm.TP) + float64(cm.FN))
	cm.Specificity = float64(cm.TN) / (float64(cm.TN) + float64(cm.FP))
	cm.F1 = 2 * ((cm.Precicion * cm.Recall) / (cm.Precicion + cm.Recall))
	cm.FPR = cm.FN / (cm.FN + cm.TN)
}

func evaluate(k int, bc *bayesColor, threads int, classifier func(uint32, uint32, uint32) bool) confusionMatrix {

	cm := newConfusionMatrix()

	c := make(chan confusionMatrix)
	imageIndex := make(chan int)

	// A thread that saturates the other threads
	go func(imageIndex chan int) {
		for j := range bc.Blocks[k] {
			imageIndex <- j
		}
		close(imageIndex)
	}(imageIndex)

	done := make(chan bool)

	go func(c chan confusionMatrix, done chan bool) {
		doneCounter := 0
		for range done {
			doneCounter++
			if doneCounter == threads {
				close(c)
			}
		}
	}(c, done)

	for i := 0; i < threads; i++ {
		go checkImage(bc.Blocks[k], bc.BlocksGT[k], c, imageIndex, done, classifier)
	}

	for v := range c {
		cm.TP += v.TP
		cm.TN += v.TN
		cm.FP += v.FP
		cm.FN += v.FN
		cm.Total += v.Total
		cm.TotalNegative += v.TotalNegative
		cm.TotalPositive += v.TotalPositive
	}

	return cm
}

func checkImage(block, blockGT []string, c chan confusionMatrix, imageIndex chan int, done chan bool, classifier func(uint32, uint32, uint32) bool) {
	for i := range imageIndex {
		cm := newConfusionMatrix()

		// Classify image
		sm := NewSegmenter(block[i])
		sm.Classify(classifier)

		gt := loadImage(blockGT[i])

		// Using the positive partitions, we can only measure true positives and false positives
		for _, R := range sm.positivePartitions {
			for x := R[0].x; x < R[2].x; x++ {
				for y := R[0].y; y < R[1].y; y++ {
					r, _, _, _ := gt.At(x, y).RGBA()
					r = r / 256

					if r > 100 {
						cm.TP++
					} else {
						cm.FP++
					}
				}
			}
		}

		// We must figure out the TN and FN
		// By counting the number of total positives and negatives and subtraction the TP and FP
		for x := 0; x < sm.img.Bounds().Max.X; x++ {
			for y := 0; y < sm.img.Bounds().Max.Y; y++ {
				r, _, _, _ := gt.At(x, y).RGBA()
				r = r / 256

				cm.Total++

				// Assume gt is white here
				if r > 100 {
					cm.TotalPositive++
				} else {
					cm.TotalNegative++
				}
			}
		}

		cm.TN = cm.TotalNegative - cm.FP
		cm.FN = cm.TotalPositive - cm.TP

		c <- cm
	}

	done <- true
}
