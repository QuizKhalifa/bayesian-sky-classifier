
BINARY_NAME=main.out

build:
	go build -o ${BINARY_NAME} main.go bayes_classifier.go compare.go image_segmenter.go probability.go

run:
	go build -o ${BINARY_NAME} main.go bayes_classifier.go compare.go image_segmenter.go probability.go
	chmod +x ./${BINARY_NAME}
	./${BINARY_NAME} -v -Q 4 -k 4 -b 

clean:
	go clean
	rm ${BINARY_NAME}
